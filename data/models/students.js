const mongoose = require('mongoose');

const Schema = mongoose.Schema;

module.exports = new Schema({
  name: String,
  surname: String,
  dateOfBirth: { type: Date, default: Date.now },
  paymentMethods: Array,
});
