const Models = require('./models');
const Schemas = require('./schemas');

module.exports = {
  Models,
  Schemas,
};
