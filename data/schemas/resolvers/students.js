const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  studentsQuery: {
    getStudents: async (parent, args, { Students }) => {
      const students = await Students.find({}).collation({ locale: 'en', strength: 2 })
        .sort({
          name: 'asc',
        });
      let options = { year: 'numeric', month: '2-digit', day: '2-digit' };
      return students.map((object) => {
        const date = new Date(object.dateOfBirth);
        const dateOfBirth = date.toLocaleDateString('en-US', options);

        const student = Object.assign({},
          object.toObject(),
          { dateOfBirth },
        );
        return student;
      });
    },
  },
  studentMutation: {
    updateMethod: async (parent, { id, data }, { Students }) => {
      const paymentMethods = JSON.parse(data);
      const model = await Students.findOne(new ObjectId(id));
      model.paymentMethods = paymentMethods;
      model.save();
      return model;
    },
  },
};
