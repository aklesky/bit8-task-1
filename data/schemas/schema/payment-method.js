const PaymentMethod = `
  type PaymentMethod {
    id: String
    type: String
    amount: String
  }
`;

module.exports = PaymentMethod;
