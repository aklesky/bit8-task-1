const PaymentMethod = require('./payment-method');

const Students = `
  type Student {
    _id: ID!
    name: String
    surname: String
    dateOfBirth: String
    paymentMethods: [PaymentMethod]
  }

  type studentsQuery {
    getStudents: [Student]
  }

  type studentMutation {
    updateMethod(id: String, data: String): Student
  }

  schema {
    query: studentsQuery
    mutation: studentMutation
  }

`;

module.exports = () => [Students, PaymentMethod];
