const { makeExecutableSchema } = require('graphql-tools');

const students = require('./schema/students');
const { studentsResolver } = require('./resolvers');

const executableSchema = makeExecutableSchema({
  typeDefs: [students],
  resolvers: studentsResolver,
});
module.exports = executableSchema;
