const faker = require('faker');
const mongoConnection = require('../config/mongo');
const uuidv4 = require('uuid/v4');

const dbConnection = mongoConnection();


const context = dbConnection.getContext();
const model = context.Students;
const students = [];

for (i = 0; i < 1000; i++) {
  const gender = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
  const object = {
    name: faker.name.firstName(gender),
    surname: faker.name.lastName(gender),
    dateOfBirth: faker.date.past(),
    paymentMethods: [
      {
        id: uuidv4(),
        type: faker.helpers.createTransaction().name,
        amount: faker.finance.amount(),
      },
      {
        id: uuidv4(),
        type: faker.helpers.createTransaction().name,
        amount: faker.finance.amount(),
      },
    ],
  };
  students.push(object);
}

model.create(students, (err, res) => {
  if (err) {
    throw err;
  }
  dbConnection.closeConnection();
});
