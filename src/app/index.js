import {
  dataTableListener,
  editButtonListener,
} from './listeners';

import {
  detailsTpl,
  formTpl,
  templateEngine,
} from './templates';

import {
  query,
  mutation,
} from './queries';

import {
  dataColumns,
  detailsColumns,
} from './columns';

import {
  en,
} from './translations';


module.exports = {
  en,
  query,
  mutation,
  dataTableListener,
  editButtonListener,
  dataColumns,
  detailsColumns,
  detailsTpl,
  formTpl,
  templateEngine,
};
