import gql from 'graphql-tag';

const mutation = gql`
mutation ($id: String!, $data: String) {
    updateMethod(id: $id, data: $data) {
        _id
        name
        surname
        dateOfBirth
        paymentMethods {
          id
          type
          amount
        }
    }
  }
`;

const query = gql`
    query studentsQuery {
      getStudents {
        _id
        name
        surname
        dateOfBirth
        paymentMethods {
          id
          type
          amount
        }
      }
    }
  `;

module.exports = {
  query,
  mutation,
};
