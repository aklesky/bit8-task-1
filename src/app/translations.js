module.exports = {
  en: {
    save: 'Save',
    edit: 'Edit',
    loading: 'Please wait...',
    name: 'First Name',
    lastName: 'Last Name',
    dob: 'DOB',
    id: 'ID',
    type: 'Type',
    amount: 'Amount',
  },
};
