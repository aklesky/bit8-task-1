const detailsTpl = require('./details.html');
const formTpl = require('./form.html');


const templateEngine = (object, template) => {
  let string = template;
  Object.keys(object).forEach((key) => {
    const pattern = new RegExp(`{{${key}}}`, 'ig');
    if (template.match(pattern, template)) {
      string = string.replace(pattern, object[key]);
    }
  });
  return string;
};

module.exports = {
  detailsTpl,
  formTpl,
  templateEngine,
};
