import {
  mutation,
} from '../queries';

const onSaveDataRecord = (dataTableRow, detailsTable, currentData, client) => {
  const paymentMethods = [];
  const editableForms = document
    .querySelectorAll(`form[data-payment-form-id='${currentData._id}']`);
  if (!editableForms.length) {
    return false;
  }
  editableForms.forEach((node) => {
    const id = node.getAttribute('id');
    const parent = node.parentNode;
    const row = detailsTable.row(parent.parentNode);
    const formData = new FormData(document.forms[id]);
    const amount = formData.get('amount');
    const object = {
      id: formData.get('id'),
      type: formData.get('type'),
      amount,
    };
    parent.innerHTML = amount;

    paymentMethods.push(object);
    row.data(object).draw();
    delete document.forms[id];
  });
  return client.mutate({ mutation, variables: { id: currentData._id, data: JSON.stringify(paymentMethods) } })
    .then((response) => {
      const updatedData = response.data.updateMethod;
      console.log('Updated Data from the server');
      console.log(updatedData);
      console.log('==============================');
      dataTableRow.data({
        ...currentData,
        paymentMethods,
      });
      return true;
    });
};

module.exports = {
  onSaveDataRecord,
};
