import {
  detailsTpl,
  formTpl,
  templateEngine,
} from '../templates';

import {
  en,
} from '../translations';

import {
  detailsColumns,
} from '../columns';

import {
  onSaveDataRecord,
} from './actions';

const editButtonListener = (e, detailsTable, dataTableRow, currentData, client) => {
  const button = e.target;
  const editableElements = document
    .querySelectorAll(`table[id='${currentData._id}'] td.editable`);


  if (button.hasAttribute('data-save')) {
    button.removeAttribute('data-save');
    const action = onSaveDataRecord(dataTableRow, detailsTable, currentData, client);
    if (!action) {
      button.innerText = en.edit;
    } else {
      button.innerText = en.loading;
      action.then(() => {
        button.innerText = en.edit;
      });
    }
  } else {
    button.setAttribute('data-save', true);
    button.innerText = en.save;
    editableElements.forEach((node) => {
      const parent = node.parentNode;
      const rowData = detailsTable.row(parent).data();
      const { _id } = currentData;
      const object = Object.assign({}, rowData, { _id });
      node.innerHTML = templateEngine(object, formTpl);
    });
  }
};

const dataTableListener = (e, dataTable, client) => {
  const target = e.target;
  if (target.nodeName.toLowerCase() === 'td' && target.className.toLowerCase().trim() === 'details-ctrl') {
    const parent = target.parentNode;
    const dataTableRow = dataTable.row(parent);
    const currentData = dataTableRow.data();

    if (parent.classList.contains('opened')) {
      dataTableRow.child.hide();
      parent.classList.remove('opened');
    } else {
      parent.classList.add('opened');
      dataTableRow.child(templateEngine(currentData, detailsTpl)).show();
    }
    console.log(currentData);
    const detailsTable = $(`[data-details][id='${currentData._id}']`).DataTable({
      data: currentData.paymentMethods,
      columns: detailsColumns,
    });
    const button = document
      .querySelector(`button[id='${currentData._id}']`);
    if (button) {
      button.innerText = en.edit;
      button.addEventListener('click', (e) => editButtonListener(e, detailsTable, dataTableRow, currentData, client));
    }
  }
};

module.exports = {
  dataTableListener,
  editButtonListener,
};
