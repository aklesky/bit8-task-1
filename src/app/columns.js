import {
  en,
} from './translations';

module.exports = {
  detailsColumns: [
    { data: 'id', title: en.id },
    { data: 'type', title: en.type },
    { data: 'amount', title: en.amount, className: 'editable' },
  ],
  dataColumns: [
    {
      className: 'details-ctrl',
      orderable: false,
      data: null,
      defaultContent: '',
    },
    { data: 'name', title: en.name },
    { data: 'surname', title: en.lastName },
    { data: 'dateOfBirth', title: en.dob },
  ],
};
