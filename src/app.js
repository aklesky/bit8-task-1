
import { ApolloClient } from 'apollo-client';

const jQuery = require('jquery');

window.$ = jQuery;
window.jQuery = jQuery;
const $ = jQuery;

require('datatables.net-bs');

require('datatables.net');
require('./static/css/app.scss');

import {
  dataTableListener,
  dataColumns,
  query,
} from './app/';

const client = new ApolloClient();

const dataQuery = client.query({
  query,
});


window.onload = () => {
  dataQuery.then((response) => {
    const data = response.data.getStudents;
    const dataTable = $('[data-table]').DataTable({
      data,
      columns: dataColumns,
      order: [[1, 'asc']],
    });
    document
      .querySelector('[data-table] > tbody')
      .addEventListener('click', (e) => dataTableListener(e, dataTable, client), false);
  });
};
