# README #
Bit8 Task 1

### Quick summary ###

***When clicking the save changes button, all information must be stored (you decide where, please provide a reason of the decision).***
even if it's a simple task with fake data it's still sort of client's data and should be persisted, 
reason why I choose mongo was just to show the demo how easy it is to communicate with database by graphql

one thousand records have been generated with [faker](https://github.com/marak/Faker.js/)
Frontend part is done by React and Apollo-React.
Backend/Frontend middleware is fully working with apollo-express-server, graphql and mongo database.

### FEATURES ###

* simple modular architecture
* Mongo Database communication though graphql and apollo client
* Updating the payment methods for a student is done by vanilla js and apollo client
* By Clicking on INFO icon it will show or hide details
* Edit/Save button will show input fields for updating the amounts and will update the record in the database
* By Clicking Save the data will be updated in the table and database and after refreshing window it would show updated information as well



### Demo ###
for the demo purpose app is connected to [MLab](https://mlab.com) and mongoose config file is configured 
(**for local environment please check the documentation bellow**)

[APP DEMO](https://aklesky-bit8-task1.herokuapp.com/)


**how to run the app locally you will find  documentation down by readme**


### CORE ###
* [apollo-client](https://github.com/apollographql/apollo-client)
* [jquery](https://jquery.com/)
* [jquery-ui](https://jqueryui.com/)
* [datatables.net](https://datatables.net)
* [graphql](http://graphql.org/)
* [mongo](https://www.mongodb.com/)
* [apollo-express-server](https://github.com/apollographql/apollo-server)
* [mongoose](http://mongoosejs.com/)

### UI ### 
* [bootstrap3](http://getbootstrap.com/)

# Project Structure
* src
* src/app
* src/app/listeners
* src/app/actions
* src/app/templates
* src/app.js (**Entrypoint**)
* src/index.html (**Main Index html file**)
* data
* data/models (**Mongo database model**)
* data/schemas (**Graphql Schemas**)
* config (**Apollo Express Server, Mongoose initialization**)
* config/env




# Local installation / dev

* install npm packages
```sh
npm i
```
* edit <project_dir>/config/env/config.json
```code
{
  "mongoUri": "mongodb://<username>:<password>@<host>:<port>/<database>",
  ...
}
* seed your local database

```code
npm run seed
```
```
* run shell command
```sh
npm run dev
```
* express is configured to listen by default port 3000
* graphiql is available only for dev environment and available by default with : http://localhost:3000/graphiql
* to view the default query run this snippet on http://localhost:3000/graphiql

```code
{
  getStudents {
    name
    surname
    dateOfBirth
    paymentMethods {
      id
      type
      amount
    }
  }
}
```


### NOT INCLUDED ###
* manifest.json
* favicon, apple icons, chrome icons
* service workers
* UI tests


### Node Version ###
* node v8.1.1
* npm 5.0.3

### Continuous Integration and Delivery ###
* Circle CI ( eslint test)
* Circle CI Continuous Delivery to Heroku
