const WebpackDevServer = require('webpack-dev-server');
const webpack = require('webpack');
const devWebPackConfig = require('./webpack.config.dev');
const config = require('./env/config');

module.exports = () => {
  const compiler = webpack(devWebPackConfig);


  const app = new WebpackDevServer(compiler, {
    proxy: {
      [config.graphqlApi]: `${config.host}:${config.devGraphQLPort}`,
      [config.graphiql]: `${config.host}:${config.devGraphQLPort}`,
    },
  });

  app.listen(config.port, () => {
  });
};
