const mongoose = require('mongoose');
const paths = require('./env/paths');
const config = require('./env/config.json');

const Data = require(paths.dataPath);

mongoose.Promise = global.Promise;

const Students = mongoose.model('Students', Data.Models.Students);

module.exports = () => {
  const connection = mongoose.connect(config.mongoUri, (err) => {
    if (err) {
      throw err;
    }
  });
  const closeConnection = () => connection.disconnect();
  const getContext = () => ({
    Students,
  });
  return {
    getContext,
    closeConnection,
  };
};
